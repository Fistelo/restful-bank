package com.fistelo.bananabank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BananabankApplication {
	public static void main(String[] args) {
		SpringApplication.run(BananabankApplication.class, args);
	}
}
