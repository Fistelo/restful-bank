package com.fistelo.bananabank.services;

import com.fistelo.bananabank.domain.User;
import com.fistelo.bananabank.repositories.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class UserServiceImpl implements UserService {
    private UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User getUserById(long userId) {
        Optional<User> user = userRepository.findById(userId);
        if(user.isPresent()){
            return user.get();
        } else {
            log.warn("There is no user with provided id");
            return null;
        }
    }

    @Override
    public void saveUser(User user) {
        userRepository.save(user);
    }
}
