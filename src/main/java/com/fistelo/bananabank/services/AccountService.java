package com.fistelo.bananabank.services;

import com.fistelo.bananabank.exceptions.NoMoneyException;
import com.fistelo.bananabank.exceptions.NotFoundException;
import com.fistelo.bananabank.api.model.WithdrawInfoDTO;

import javax.security.sasl.AuthenticationException;
import java.math.BigDecimal;

public interface AccountService {
    BigDecimal getUserBalance(long userId);
    void increaseUserBalance(long userId, BigDecimal value) throws NotFoundException;
    void decreaseUserBalance(long userId, WithdrawInfoDTO withdrawInfo) throws NotFoundException, NoMoneyException, AuthenticationException;
}
