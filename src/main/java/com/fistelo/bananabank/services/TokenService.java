package com.fistelo.bananabank.services;

import com.fistelo.bananabank.domain.Token;
import com.fistelo.bananabank.domain.User;
import com.fistelo.bananabank.repositories.TokenRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@Slf4j
public class TokenService {

    public static final long DELETE_TOKENS_TIME = 1000 * 20;
    private UserService userService;
    private TokenRepository tokenRepository;

    public TokenService(UserService userService, TokenRepository tokenRepository) {
        this.userService = userService;
        this.tokenRepository = tokenRepository;
    }

    @Scheduled(fixedDelay = DELETE_TOKENS_TIME)
    public void expireTokens(){
        log.info("Deleting expired tokens");
        List<Token> tokens = new ArrayList<>();
        tokenRepository.findAll().forEach(tokens::add);
        tokens.stream().filter(Token::isExpired).forEach(t -> {
            User user = t.getUser();
            user.setToken(null);
            userService.saveUser(user);
        });
    }

    private String generateToken(){
        return UUID.randomUUID().toString();
    }

    public String getToken(User user) {
        Token currentToken = user.getToken();
        if (currentToken == null){
            return createNewToken(user);
        } else {
            return currentToken.getValue();
        }
    }

    private String createNewToken(User user){
        String generatedToken = generateToken();
        Token newToken = new Token(generatedToken, user);
        user.setToken(newToken);
        userService.saveUser(user);
        return generatedToken;
    }
}
