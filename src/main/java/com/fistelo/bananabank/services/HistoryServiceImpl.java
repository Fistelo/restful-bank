package com.fistelo.bananabank.services;

import com.fistelo.bananabank.api.converters.TransactionToDTO;
import com.fistelo.bananabank.api.model.TransactionDTO;
import com.fistelo.bananabank.domain.Account;
import com.fistelo.bananabank.domain.Action;
import com.fistelo.bananabank.domain.MoneyTransaction;
import com.fistelo.bananabank.repositories.AccountRepository;
import com.fistelo.bananabank.repositories.TransactionRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class HistoryServiceImpl implements HistoryService {

    private TransactionRepository transactionRepository;
    private AccountRepository accountRepository;
    private TransactionToDTO transactionToDTO;

    public HistoryServiceImpl(TransactionRepository transactionRepository,
                              AccountRepository accountRepository, TransactionToDTO transactionToDTO) {
        this.transactionRepository = transactionRepository;
        this.accountRepository = accountRepository;
        this.transactionToDTO = transactionToDTO;
    }

    public void addToHistory(Account account, BigDecimal value, Action action) {
        MoneyTransaction transaction = new MoneyTransaction();
        transaction.setAction(action);
        transaction.setValue(value);
        account.addTransactionMutually(transaction);
        transactionRepository.save(transaction);
    }

    @Override
    public List<TransactionDTO> getWholeHistory(long userId) {
        Account account = accountRepository.findAccountByUserId(userId);
        if (account == null) return null;

        return account.getTransactions().stream()
                .map(transactionToDTO::convert)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }
}
