package com.fistelo.bananabank.services;
import com.fistelo.bananabank.domain.User;
import com.fistelo.bananabank.exceptions.NoMoneyException;
import com.fistelo.bananabank.exceptions.NotFoundException;
import com.fistelo.bananabank.api.model.WithdrawInfoDTO;
import com.fistelo.bananabank.domain.Account;
import com.fistelo.bananabank.domain.Action;
import com.fistelo.bananabank.repositories.AccountRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.security.sasl.AuthenticationException;
import java.math.BigDecimal;

@Service
@Slf4j
public class AccountServiceImpl implements AccountService {

    private AccountRepository accountRepository;

    private HistoryService historyService;
    private UserService userService;

    public AccountServiceImpl(AccountRepository accountRepository,
                              HistoryService historyService, UserService userService) {
        this.accountRepository = accountRepository;
        this.historyService = historyService;
        this.userService = userService;
    }

    @Override
    public BigDecimal getUserBalance(long userId) {
        Account account = accountRepository.findAccountByUserId(userId);
        return account == null ?  null : account.getBalance();
    }

    @Override
    @Transactional
    public void increaseUserBalance(long userId, BigDecimal value) throws NotFoundException {
        Account account = accountRepository.findAccountByUserId(userId);
        if (account == null) {
            log.warn("There is no account associated with given user id.");
            throw new NotFoundException();
        }
        account.setBalance(account.getBalance().add(value));
        saveBalanceChangesInDatabase(account, value, Action.PAYMENT);
    }

    @Override
    @Transactional
    public void decreaseUserBalance(long userId, WithdrawInfoDTO withdrawInfo) throws NotFoundException, NoMoneyException, AuthenticationException {
        Account account = accountRepository.findAccountByUserId(userId);
        if (account == null || account.getUser() == null) throw new NotFoundException();
        User user = account.getUser();

        if (checkToken(withdrawInfo.getToken(), user)){
            final BigDecimal resultBalance = account.getBalance().subtract(withdrawInfo.getValue());
            if( resultBalance.compareTo(BigDecimal.ZERO) == -1){
                throw new NoMoneyException();
            }
            account.setBalance(resultBalance);
            saveBalanceChangesInDatabase(account, withdrawInfo.getValue(), Action.WITHDRAW);
            user.setToken(null);
            userService.saveUser(user);
        } else
            throw new AuthenticationException();
    }

    private void saveBalanceChangesInDatabase(Account account, BigDecimal value, Action action){
        accountRepository.save(account);
        historyService.addToHistory(account, value, action);
    }

    private boolean checkToken(String token, User user){
        if(user.getToken() == null){
            log.warn("Problem validating token, user token in null");
        } else {
            if (token.equals(user.getToken().getValue()))
                return true;
        }
        return false;
    }
}
