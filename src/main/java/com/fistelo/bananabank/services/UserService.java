package com.fistelo.bananabank.services;

import com.fistelo.bananabank.domain.User;

public interface UserService {
    User getUserById(long userId);
    void saveUser(User user);
}
