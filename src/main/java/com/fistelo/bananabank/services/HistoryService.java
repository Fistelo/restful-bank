package com.fistelo.bananabank.services;

import com.fistelo.bananabank.api.model.TransactionDTO;
import com.fistelo.bananabank.domain.Account;
import com.fistelo.bananabank.domain.Action;

import java.math.BigDecimal;
import java.util.List;

public interface HistoryService {

    void addToHistory(Account account, BigDecimal value, Action action);
    List<TransactionDTO> getWholeHistory(long userId);
}
