package com.fistelo.bananabank.repositories;

import com.fistelo.bananabank.domain.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {
    Optional <User> findByName(@Param("name") String name);
}
