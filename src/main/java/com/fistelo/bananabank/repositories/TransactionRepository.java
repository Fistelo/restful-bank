package com.fistelo.bananabank.repositories;

import com.fistelo.bananabank.domain.MoneyTransaction;
import org.springframework.data.repository.CrudRepository;

public interface TransactionRepository extends CrudRepository<MoneyTransaction, Long> {
}
