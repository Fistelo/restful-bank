package com.fistelo.bananabank.repositories;

import com.fistelo.bananabank.domain.Token;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface TokenRepository extends CrudRepository<Token, Long> {
    Optional<Token> findTokenByValue(@Param("value") String value);
}
