package com.fistelo.bananabank.repositories;

import com.fistelo.bananabank.domain.Account;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface AccountRepository extends CrudRepository<Account, Long> {

    @Query("from Account a join a.user u where u.id=:userId")
    Account findAccountByUserId(@Param("userId") long userId);
}
