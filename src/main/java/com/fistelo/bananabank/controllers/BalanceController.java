package com.fistelo.bananabank.controllers;

import com.fistelo.bananabank.exceptions.NoMoneyException;
import com.fistelo.bananabank.exceptions.NotFoundException;
import com.fistelo.bananabank.api.model.MoneyValueDTO;
import com.fistelo.bananabank.api.model.WithdrawInfoDTO;
import com.fistelo.bananabank.services.AccountService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.security.sasl.AuthenticationException;
import java.math.BigDecimal;
import java.util.Objects;

@RestController
@RequestMapping(ApiUrls.BALANCE_BASE_URL)
public class BalanceController {

    private final AccountService accountService;

    public BalanceController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<MoneyValueDTO> getBalance(@PathVariable("id") long userId){
        BigDecimal balance = accountService.getUserBalance(userId);

        if(Objects.isNull(balance)){
            return ResponseEntity.notFound().build();
        } else
            return new ResponseEntity<>(new MoneyValueDTO(balance), HttpStatus.OK);
    }

    @PostMapping("/increase/user/{id}")
    public ResponseEntity<String> increaseBalance(@PathVariable("id") long userId,
                                  @RequestBody MoneyValueDTO moneyDifference) {
        try {
            accountService.increaseUserBalance(userId, moneyDifference.getValue());
        } catch(NotFoundException e) {
            return new ResponseEntity("There is no user associated with given user id.", HttpStatus.NOT_FOUND);
        }
            return ResponseEntity.ok().build();
    }

    @PostMapping("/decrease/user/{id}")
    public ResponseEntity<String> withdrawMoney(@PathVariable("id") long userId,
                                                       @RequestBody WithdrawInfoDTO withdrawInfo) {
        try {
            accountService.decreaseUserBalance(userId, withdrawInfo);
            return ResponseEntity.ok().build();
        } catch (NotFoundException e) {
            return new ResponseEntity("Account or user with provided id not found.", HttpStatus.NOT_FOUND);
        } catch (NoMoneyException e) {
            return new ResponseEntity("You've got too little money on Your account to process  withdrawal.", HttpStatus.UNPROCESSABLE_ENTITY);
        } catch (AuthenticationException e) {
            return new ResponseEntity("Your token is not valid or expired.", HttpStatus.UNAUTHORIZED);
        }
    }
}
