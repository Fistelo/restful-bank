package com.fistelo.bananabank.controllers;


public class ApiUrls {

    public static final String BALANCE_BASE_URL = "/api/v1/balance";
    public static final String HISTORY_BASE_URL = "/api/v1/history";
    public static final String TOKEN_BASE_URL = "/api/v1/tokens";

    private ApiUrls(){
        throw new AssertionError();
    }
}
