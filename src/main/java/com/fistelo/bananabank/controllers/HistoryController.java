package com.fistelo.bananabank.controllers;

import com.fistelo.bananabank.api.model.TransactionDTO;
import com.fistelo.bananabank.services.HistoryService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping(ApiUrls.HISTORY_BASE_URL)
public class HistoryController {

    private HistoryService historyService;

    public HistoryController(HistoryService historyService) {
        this.historyService = historyService;
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<List<TransactionDTO>> getUserHistory(@PathVariable("id") long userId) {
        List<TransactionDTO> moneyTransactionList = historyService.getWholeHistory(userId);
        if (Objects.isNull(moneyTransactionList)){
            return ResponseEntity.notFound().build();
        } else
            return new ResponseEntity<>(moneyTransactionList, HttpStatus.OK);
    }
}
