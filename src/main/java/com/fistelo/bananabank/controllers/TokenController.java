package com.fistelo.bananabank.controllers;

import com.fistelo.bananabank.api.model.TokenDTO;
import com.fistelo.bananabank.domain.User;
import com.fistelo.bananabank.services.TokenService;
import com.fistelo.bananabank.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping(ApiUrls.TOKEN_BASE_URL)
@Slf4j
public class TokenController {

    UserService userService;
    TokenService tokenService;

    public TokenController(UserService userService, TokenService tokenService) {
        this.userService = userService;
        this.tokenService = tokenService;
    }

    @PostMapping("user/{id}")
    public ResponseEntity<TokenDTO> getToken(@PathVariable("id") long userId, Principal principal){
        try{
            User user = userService.getUserById(userId);
            if(user.getName().equals(principal.getName())){
                TokenDTO tokenDTO = new TokenDTO(tokenService.getToken(user));
                return new ResponseEntity<>(tokenDTO, HttpStatus.OK);
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new TokenDTO("Username doesn't match provided id!" ));
            }
        } catch(NullPointerException e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new TokenDTO("User with provided id not found"));
        }
    }
}
