package com.fistelo.bananabank.exceptions;

public class NoMoneyException extends Exception {

    public NoMoneyException() {
        super("Not enough money on user's account.");
    }
}