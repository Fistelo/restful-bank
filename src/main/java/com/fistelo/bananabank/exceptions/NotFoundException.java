package com.fistelo.bananabank.exceptions;

public class NotFoundException extends Exception {

    public NotFoundException() {
        super("Resource not found in the database.");
    }
}