package com.fistelo.bananabank.api.converters;

import com.fistelo.bananabank.api.model.TransactionDTO;
import com.fistelo.bananabank.domain.Action;
import com.fistelo.bananabank.domain.MoneyTransaction;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class TransactionToDTO implements Converter<MoneyTransaction, TransactionDTO> {

    @Nullable
    @Override
    public TransactionDTO convert(MoneyTransaction source) {
        if(source == null)
            return null;
        final TransactionDTO transactionDTO = new TransactionDTO();
        transactionDTO.setValue(source.getValue());

        try {
            Action sourceAction = source.getAction();
            transactionDTO.setType(sourceAction.toString().toLowerCase());
        } catch (NullPointerException e){
            log.warn("Problem while converting. MoneyTransaction action is null!");
            transactionDTO.setType("unknown");
        }

        return transactionDTO;
    }
}
