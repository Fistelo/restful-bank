package com.fistelo.bananabank.api.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class TransactionDTO {
    String type;
    BigDecimal value;
}
