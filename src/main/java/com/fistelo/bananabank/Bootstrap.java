package com.fistelo.bananabank;

import com.fistelo.bananabank.domain.Account;
import com.fistelo.bananabank.domain.Action;
import com.fistelo.bananabank.domain.MoneyTransaction;
import com.fistelo.bananabank.domain.User;
import com.fistelo.bananabank.repositories.AccountRepository;
import com.fistelo.bananabank.repositories.TransactionRepository;
import com.fistelo.bananabank.repositories.UserRepository;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

@Component
public class Bootstrap implements ApplicationListener<ContextRefreshedEvent> {

    UserRepository userRepository;
    AccountRepository accountRepository;
    TransactionRepository transactionRepository;
    BCryptPasswordEncoder bCryptEncoder;

    public Bootstrap(UserRepository userRepository, AccountRepository accountRepository, TransactionRepository transactionRepository) {
        this.userRepository = userRepository;
        this.accountRepository = accountRepository;
        this.transactionRepository = transactionRepository;
        bCryptEncoder = new BCryptPasswordEncoder();
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        initalize();
    }

    private void initalize() {
        User user1 = new User();
        user1.setName("Auser");
        user1.setPassword(bCryptEncoder.encode("Auser"));
        user1.setRole("USER");
        User user2 = new User();
        user2.setName("Buser");
        user2.setPassword(bCryptEncoder.encode("Buser"));
        user2.setRole("USER");

        Account acc1 = new Account();
        acc1.setBalance(new BigDecimal(30));
        Account acc2 = new Account();
        acc2.setBalance(new BigDecimal(4000));

        user1.setAccountMutually(acc1);
        user2.setAccountMutually(acc2);

        userRepository.saveAll(Arrays.asList(user1,user2));
        accountRepository.saveAll(Arrays.asList(acc1,acc2));

    }
}
