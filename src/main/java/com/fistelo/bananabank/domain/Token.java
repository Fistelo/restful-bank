package com.fistelo.bananabank.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@NoArgsConstructor
public class Token {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter @Setter
    Long id;
    @Getter @Setter
    String value;
    @Getter @Setter
    LocalDateTime date;
    @Getter @Setter
    @OneToOne
    User user;

    public Token (String value, User user){
        this.value = value;
        this.user = user;
        date = LocalDateTime.now().plusMinutes(2);
    }

    public boolean isExpired(){
        if(date.compareTo(LocalDateTime.now()) < 0)
            return true;
        return false;
    }
}
