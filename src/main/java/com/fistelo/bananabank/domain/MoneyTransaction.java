package com.fistelo.bananabank.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@EqualsAndHashCode(exclude = "account")
public class MoneyTransaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter @Setter
    private Long id;
    @Getter @Setter
    private BigDecimal value;
    @Enumerated(value = EnumType.STRING)
    @Getter @Setter
    private Action action;
    @ManyToOne
    @Getter @Setter
    private Account account;
}
