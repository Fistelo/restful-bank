package com.fistelo.bananabank.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;

@Slf4j
@Entity
@Data
@EqualsAndHashCode(exclude = "account")
public class User {
    @Getter @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Getter
    private String name;
    @Getter @Setter
    private String password;
    @Getter @Setter
    private String role;
    @Getter @Setter
    @OneToOne(cascade = CascadeType.ALL)
    private Account account;
    @Getter @Setter
    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private Token token;

    public void setAccountMutually(Account account){
        if(account != null){
            this.account = account;
            account.setUser(this);
        } else {
            log.warn("Can't set user to account when account is empty.");
        }
    }
}
