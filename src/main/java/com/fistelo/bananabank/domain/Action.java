package com.fistelo.bananabank.domain;

public enum Action {
    WITHDRAW, PAYMENT
}
