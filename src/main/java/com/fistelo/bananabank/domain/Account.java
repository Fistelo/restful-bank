package com.fistelo.bananabank.domain;

import lombok.*;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Slf4j
@Entity
@EqualsAndHashCode(exclude = "user")
public class Account {
    @Getter @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Getter @Setter
    private BigDecimal balance;
    @Getter @Setter
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "account")
    private List<MoneyTransaction> transactions = new ArrayList<>();
    @Getter @Setter
    @OneToOne
    private User user;

    public void addTransactionMutually(MoneyTransaction transaction){
        if(transaction != null) {
            this.transactions.add(transaction);
            transaction.setAccount(this);
        } else {
            log.warn("Can't add transaction to account history when transaction is empty.");
        }
    }
}
