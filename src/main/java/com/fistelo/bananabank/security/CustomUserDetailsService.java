package com.fistelo.bananabank.security;

import com.fistelo.bananabank.domain.User;
import com.fistelo.bananabank.repositories.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class CustomUserDetailsService implements UserDetailsService {

    private UserRepository userRepository;

    public CustomUserDetailsService(UserRepository userRepository){
        this.userRepository=userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        Optional<User> optionalUser = userRepository.findByName(userName);
        if (!optionalUser.isPresent()) {
            log.warn("User with provided username not found");
            return null;
        }
        User user = optionalUser.get();
        return new org.springframework.security.core.userdetails.User(user.getName(), user.getPassword(), getAuthority(user));
    }

    private List<GrantedAuthority> getAuthority(User user){
        String role = user.getRole();
        if(role == null) {
            log.warn("User You trying to add has no role assigned.");
            return null;
        }
        GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(role);
        return Arrays.asList(grantedAuthority);
    }
}
