package com.fistelo.bananabank.services;

import com.fistelo.bananabank.api.model.WithdrawInfoDTO;
import com.fistelo.bananabank.domain.Account;
import com.fistelo.bananabank.domain.Token;
import com.fistelo.bananabank.domain.User;
import com.fistelo.bananabank.exceptions.NoMoneyException;
import com.fistelo.bananabank.repositories.AccountRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class AccountServiceImplTest {

    public static final String TOKEN_VALUE = "token";

    @Mock
    AccountRepository accountRepository;
    @Mock HistoryServiceImpl historyService;
    @Mock UserService userService;

    AccountService accountService;
    Token token;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        token = new Token();
        token.setValue(TOKEN_VALUE);
        accountService = new AccountServiceImpl(accountRepository, historyService, userService);
    }

    @Test
    public void getUserBalanceTest() throws Exception {
        Account account = new Account();
        account.setBalance(new BigDecimal(45));

        when(accountRepository.findAccountByUserId(1)).thenReturn(account);
        BigDecimal balance = accountService.getUserBalance(1);

        assertEquals(balance, account.getBalance());
    }

    @Test
    public void increaseBalanceTest() throws Exception {
        Account account = new Account();
        account.setBalance(new BigDecimal(45));

        when(accountRepository.findAccountByUserId(1)).thenReturn(account);
        accountService.increaseUserBalance(1, new BigDecimal(30));

        assertEquals(account.getBalance(), new BigDecimal(75));
    }

    @Test(expected = NoMoneyException.class)
    public void whenDecreaseBalanceAndNoMoneyThrowExceptionTest() throws Exception {
        User user = new User();
        user.setToken(token);
        Account account = new Account();
        account.setBalance(BigDecimal.ONE);
        account.setUser(user);
        WithdrawInfoDTO withdrawInfo = new WithdrawInfoDTO(new BigDecimal(2), "token");

        when(accountRepository.findAccountByUserId(1)).thenReturn(account);
        accountService.decreaseUserBalance(1, withdrawInfo);
    }

    @Test
    public void decreaseBalanceTest() throws Exception {
        User user = new User();
        user.setToken(token);
        Account account = new Account();
        account.setBalance(new BigDecimal(45));
        account.setUser(user);
        WithdrawInfoDTO withdrawInfo = new WithdrawInfoDTO(new BigDecimal(15), TOKEN_VALUE);

        when(accountRepository.findAccountByUserId(1)).thenReturn(account);
        accountService.decreaseUserBalance(1, withdrawInfo);

        assertEquals(account.getBalance(), new BigDecimal(30));
    }
}