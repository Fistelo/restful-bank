package com.fistelo.bananabank.services;

import com.fistelo.bananabank.api.converters.TransactionToDTO;
import com.fistelo.bananabank.api.model.TransactionDTO;
import com.fistelo.bananabank.domain.Account;
import com.fistelo.bananabank.domain.Action;
import com.fistelo.bananabank.domain.MoneyTransaction;
import com.fistelo.bananabank.repositories.AccountRepository;
import com.fistelo.bananabank.repositories.TransactionRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class HistoryServiceImplTest {

    @Mock AccountRepository accountRepository;
    @Mock TransactionRepository transactionRepository;
    TransactionToDTO transactionToDTO;

    HistoryService historyService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        transactionToDTO = new TransactionToDTO();
        historyService = new HistoryServiceImpl(transactionRepository, accountRepository, transactionToDTO);
    }

    @Test
    public void addToHistory() throws Exception {
        Account account = new Account();
        BigDecimal value = new BigDecimal(2);
        historyService.addToHistory(account, value, Action.PAYMENT);

        assertEquals(account.getTransactions().size(), 1);
        assertEquals(account.getTransactions().get(0).getValue(), value);
    }

    @Test
    public void getWholeHistory() throws Exception {
        Account account = new Account();
        MoneyTransaction moneyTransaction = new MoneyTransaction();
        moneyTransaction.setId(1L);
        moneyTransaction.setValue(new BigDecimal(10));
        moneyTransaction.setAction(Action.PAYMENT);
        account.addTransactionMutually(moneyTransaction);

        when(accountRepository.findAccountByUserId(1)).thenReturn(account);

        List<TransactionDTO> transactions = historyService.getWholeHistory(1);

        assertEquals(transactions.size(), 1);
        assertEquals(transactions.get(0).getType(), moneyTransaction.getAction().toString().toLowerCase());
        assertEquals(transactions.get(0).getValue(), moneyTransaction.getValue());
    }

}