package com.fistelo.bananabank.services;

import com.fistelo.bananabank.domain.Token;
import com.fistelo.bananabank.domain.User;
import com.fistelo.bananabank.repositories.TokenRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.junit.Assert.*;

public class TokenServiceTest {
    public static final String TOKEN_VALUE = "token";

    @Mock UserService userService;
    @Mock TokenRepository tokenRepository;
    TokenService tokenService;

    @Before
    public void setUp(){
        tokenService = new TokenService(userService, tokenRepository);
    }
    @Test
    public void whenUserHaveTokenReturnHisToken() throws Exception {
        User user = new User();
        Token token = new Token();
        token.setValue(TOKEN_VALUE);
        user.setToken(token);

        String generatedToken = tokenService.getToken(user);
        assertEquals(generatedToken, user.getToken().getValue());
    }
}