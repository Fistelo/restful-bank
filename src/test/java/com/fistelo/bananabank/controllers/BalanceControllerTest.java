package com.fistelo.bananabank.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fistelo.bananabank.api.model.MoneyValueDTO;
import com.fistelo.bananabank.services.AccountService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class BalanceControllerTest {

    @Mock
    AccountService accountService;

    @InjectMocks
    BalanceController balanceController;

    MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        mockMvc = MockMvcBuilders.standaloneSetup(balanceController)
                .build();
    }

    @Test
    public void getCustomerById() throws Exception{
        when(accountService.getUserBalance(1)).thenReturn(new BigDecimal(30));

        mockMvc.perform(get(ApiUrls.BALANCE_BASE_URL + "/user/1")
        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.value", equalTo(30)));
    }

    @Test
    public void increaseUserBalance() throws Exception{
        MoneyValueDTO value = new MoneyValueDTO(new BigDecimal(2));

        mockMvc.perform(post(ApiUrls.BALANCE_BASE_URL + "/increase/user/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(value)))
                .andExpect(status().isOk());
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}