package com.fistelo.bananabank.api.converters;

import com.fistelo.bananabank.api.model.TransactionDTO;
import com.fistelo.bananabank.domain.Action;
import com.fistelo.bananabank.domain.MoneyTransaction;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class TransactionToDTOTest {
    private TransactionToDTO transactionToDTO;

    @Before
    public void setUp(){
        transactionToDTO = new TransactionToDTO();
    }

    @Test
    public void whenProperTypeReturnValidDTOTest() throws Exception {
        MoneyTransaction moneyTransaction = new MoneyTransaction();
        moneyTransaction.setValue(new BigDecimal(1));
        moneyTransaction.setAction(Action.WITHDRAW);

        TransactionDTO transactionDTO = transactionToDTO.convert(moneyTransaction);

        assertEquals(transactionDTO.getValue(), moneyTransaction.getValue());
        assertEquals(transactionDTO.getType().toUpperCase(), moneyTransaction.getAction().toString());
    }

    @Test
    public void whenNullTypeReturnUnknown() throws Exception {
        MoneyTransaction moneyTransaction = new MoneyTransaction();
        moneyTransaction.setValue(new BigDecimal(10));

        TransactionDTO transactionDTO = transactionToDTO.convert(moneyTransaction);
        assertEquals(transactionDTO.getType(), "unknown");
    }
}